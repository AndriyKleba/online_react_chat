import React, {Component} from "react";
import {Form, Input} from "antd";
import {LockOutlined, UserOutlined, MailOutlined, ExclamationCircleTwoTone} from "@ant-design/icons";
import {Block, Button} from "../../../components/PointComponents";
import {Link} from 'react-router-dom';

const RegisterForm = props => {
    // const onFinish = values => {
    //     console.log('Received values of form: ', values);
    // };
    //onFinish={onFinish}

    const success = false;
    const {
        values,
        touched,
        errors,
        isSubmitting,
        handleChange,
        handleBlur,
        handleSubmit,
        handleReset,
        dirty,
    } = props;


    return (
        <div>
            <div className="auth__top">
                <h2>Реєстрація</h2>
                <p>Щоб увійти в чат спотатку зареєструйтесь</p>
            </div>
            <Block>
                {!success ? (

                    <Form
                        onSubmit={handleSubmit}
                        name="normal_login"
                        className="login-form"
                        initialValues={{remember: true}}

                    >

                        <Form.Item
                            name="username"
                            rules={[{required: true, message: 'Будь-ласка введіть Ваше ім\'я'}]}
                        >
                            <Input
                                prefix={<UserOutlined className="site-form-item-icon"/>}
                                type="username"
                                placeholder="Ваше ім'я"
                                size="large"
                            />
                        </Form.Item>

                        <Form.Item
                            hasFeedback
                            name="email"
                            rules={[{required: true, message: 'Будь-ласка введіть ваш email'}]}
                        >
                            <Input prefix={<MailOutlined className="site-form-item-icon"/>}
                                   placeholder="Email"
                                   size="large"
                                   value={values.email}
                            />
                        </Form.Item>

                        <Form.Item
                            rules={[{required: true, message: 'Будь-ласка введіть Пароль'}]}
                        >
                            <Input
                                prefix={<LockOutlined className="site-form-item-icon"/>}
                                type="password"
                                placeholder="Пароль"
                                size="large"
                            />
                        </Form.Item>

                        <Form.Item
                            rules={[{required: true, message: 'Будь-ласка поворіть ваш Пароль'}]}
                        >
                            <Input
                                prefix={<LockOutlined className="site-form-item-icon"/>}
                                type="password"
                                placeholder="Повторіть пароль"
                                size="large"
                            />
                        </Form.Item>

                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="login-form-button" size="large">
                                Зареєструватися
                            </Button>
                        </Form.Item>

                        <Link className="auth__register-link" to="/login">Увійти в аккаунт</Link>

                    </Form>
                ) : (
                    <div className="auth__success-block">
                        <div>
                            <ExclamationCircleTwoTone style={{fontSize: '50px', color: '#08c'}}/>
                        </div>
                        <h3>Підтвердіть ваш аккаунт</h3>
                        <p>На вашу почту відправлено лист з силкою для підтвердження аккаунта</p>
                    </div>
                )}
            </Block>
        </div>
    )
};


export default RegisterForm;

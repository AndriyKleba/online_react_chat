import React, {Component} from "react";
import {Form, Input} from "antd";
import {LockOutlined, UserOutlined} from "@ant-design/icons";
import {Block, Button} from "../../../components/PointComponents";
import {Link} from 'react-router-dom';

class LoginForm extends Component {

    render() {
        return (
            <div>
                <div className="auth__top">
                    <h2>Увійти в аккаунт</h2>
                    <p>Будь-ласка, зайдіть в свій аккаунт</p>
                </div>
                <Block>
                    <Form
                        name="normal_login"
                        className="login-form"
                        initialValues={{remember: true}}
                        onFinish={onFinish}
                    >
                        <Form.Item
                            hasFeedback
                            validateStatus="success"
                            name="username"
                            rules={[{required: true, message: 'Please input your Username!'}]}
                        >
                            <Input prefix={<UserOutlined className="site-form-item-icon"/>}
                                   placeholder="Ваше ім'я"
                                   size="large"
                            />
                        </Form.Item>
                        <Form.Item
                            name="password"
                            rules={[{required: true, message: 'Please input your Password!'}]}
                        >
                            <Input
                                prefix={<LockOutlined className="site-form-item-icon"/>}
                                type="password"
                                placeholder="Пароль"
                                size="large"
                            />
                        </Form.Item>

                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="login-form-button" size="large">
                                Війти в аккаунт
                            </Button>
                        </Form.Item>

                        <Link className="auth__register-link" to="/register">Зареєструватися</Link>

                    </Form>
                </Block>
            </div>
        )
    }
};

const onFinish = values => {
    console.log('Received values of form: ', values);
};

export default LoginForm;

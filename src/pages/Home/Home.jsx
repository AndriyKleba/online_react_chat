import React from "react";

import {Message} from "../../components/PointComponents";
import './Home.scss';

const Home = () => {


    return (
        <section className="home">

            <Message
                avatar="https://i.pinimg.com/originals/0a/42/7f/0a427f8c57082a1d1f0da6538acabf32.jpg"
                text="Привіт в тебе не буде сухого корму? Потім тобі відам бо голодний, а хазяїн кудась пішов😔"
                date={new Date('Fri Mar 06 2020 17:05:21')}
                attachments={[
                    {
                        filename: 'image-jpg',
                        url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRIdgHC75ri5OAUjNybeRm5W95bVaQSwkEwTKYBIIczPJMRfxkJ',
                    },
                    {
                        filename: 'image-jpg',
                        url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS7CQPPfKwT5-2UTNBMqeV73zLAfJD8w7Ys7P_0Nm4tS9aSh881',
                    },
                    {
                        filename: 'image-jpg',
                        url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRdEvuWt9GaRwggfNuMtsX5vJ3r4_NbzDkIjDxCNhwY7n5xXV7v',
                    }
                ]}
            />

            <Message
                avatar="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQsBJtVjVdocG5FQC5wnGQwlQuqgm85ZuY2EMbTWaDLuPVL2_pK"
                text="Здоров, то не ти вічно гавкаєш?"
                date={new Date('Fri Mar 06 2020 15:20:21')}
                isMe={true}
                isReaded={true}
            />

        </section>
    )
};


export default Home;
